Source: coot
Maintainer: Debian Science Maintainers <debian-science-maintainers@lists.alioth.debian.org>
Uploaders:
 Morten Kjeldgaard <mok0@ubuntu.com>,
 Picca Frédéric-Emmanuel <picca@debian.org>,
 Andrius Merkys <merkys@debian.org>,
 Alex Myczko <tar@debian.org>,
Section: science
Priority: optional
Build-Depends:
 bc,
 cmake,
 debhelper-compat (= 13),
 dh-sequence-python3,
 gemmi,
 gemmi-dev,
 ghostscript,
 imagemagick | graphicsmagick-imagemagick-compat,
 libasound2-dev,
 libblas-dev,
 libboost-dev,
 libboost-iostreams-dev,
 libboost-python-dev,
 libboost-system-dev,
 libboost-thread-dev,
 libcairo-dev,
 libccp4-data,
 libclipper-dev,
 libcurl4-gnutls-dev | libcurl-dev,
 libdw-dev,
 libeigen3-dev,
 libelf-dev,
 libgl-dev,
 libglib2.0-dev,
 libglm-dev,
 libgsl-dev,
 libgtkmm-4.0-dev,
 libmmdb2-dev,
 libpng-dev,
 libpython3-dev,
 librdkit-dev,
 libsqlite3-dev,
 libssm-dev,
 libvorbis-dev,
 nanobind-dev,
 python-gi-dev,
 python3-rdkit,
 python3-requests,
 python3-setuptools,
 sharutils,
 swig,
 tao-pegtl-dev,
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/science-team/coot
Vcs-Git: https://salsa.debian.org/science-team/coot.git
Homepage: https://github.com/pemsley/coot
Rules-Requires-Root: no

Package: coot
Architecture: any
Depends:
 coot-data,
 gir1.2-gtk-4.0,
 python3-gi,
 python3-rdkit,
 refmac-dictionary,
 ${misc:Depends},
 ${python3:Depends},
 ${shlibs:Depends},
Suggests:
 coot-doc,
Description: model building program for macromolecular crystallography
 This is a program for constructing atomic models of macromolecules
 from x-ray diffraction data. Coot displays electron density maps and
 molecular models and allows model manipulations such as idealization,
 refinement, manual rotation/translation, rigid-body fitting, ligand
 search, solvation, mutations, rotamers. Validation tools such as
 Ramachandran and geometry plots are available to the user. This
 package provides a Coot build with embedded Python support.

Package: coot-data
Architecture: all
Depends:
 fonts-noto-core,
 ttf-bitstream-vera,
 ${misc:Depends},
Description: various data files needed by Coot
 This is an interactive program for fitting atomic models to
 crystallographic electron density maps. This package contains various
 data files needed by Coot.

Package: coot-doc
Architecture: all
Multi-Arch: foreign
Depends:
 ${misc:Depends},
Description: documentation for Coot
 This is an interactive program for fitting atomic models to
 crystallographic electron density maps. This package contains
 documentation source files for Coot.

Package: libcootapi-dev
Architecture: any
Section: libdevel
Depends:
 libcootapi1.1 (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: development files for libcoot
 Coot is an interactive program for fitting atomic models to
 crystallographic electron density maps.
 .
 This package contains the development files.

Package: libcootapi1.1
Architecture: any
Section: libs
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: shared library for coot
 Coot is an interactive program for fitting atomic models to
 crystallographic electron density maps.
 .
 This package contains the shared library for Coot's API.
